<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/','SiteController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/post','Admin\PostController@index');
Route::get('home/post/create','Admin\PostController@showForm')->name('post-create');
Route::post('home/post/create','Admin\PostController@create')->name('post-store');
Route::get('home/post/edit/{id}','Admin\PostController@edit')->name('post-edit');
Route::post('home/post/update/{id}','Admin\PostController@updatePost')->name('post-update');

Route::get('/{any}', function () {
    return view('post');
  })->where('any', '.*');
