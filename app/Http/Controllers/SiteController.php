<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;


class SiteController extends Controller
{
   public function index()
   {
    $posts = Post::all();
    return view('home',compact('posts'));
   }
}
