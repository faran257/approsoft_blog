<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Post;
use App\Models\Comment;
use App\Traits\ImageUploadTrait;
use Auth;

class PostController extends Controller
{
    use ImageUploadTrait;
    protected $photo = 'photo';
    /**
     * 
     * show Post form admin 
     * */
    public function index()
    {
        $posts = Post::all();
        return view('posts.index',compact('posts'));
    }
    /**
     * 
     * show Post form admin 
     * */
    public function showForm()
    {
        return view('posts.create');
    }

    /**
     * 
     * Create Post form admin 
     * */
    public function create(Request $request){
    
        $validate           = Validator::make($request->all(), [
            'photo'         => 'required',
            'title'         => 'required',
            'description'   =>'required'
         ])->validate();
         $posts             = Post::create([
             'image'        =>$this->uploadImage($request->photo),
             'title'        =>$request->title,
             'description'  =>$request->input('description')
         ]);
         if ($posts){
             return redirect('/home');
         } else {
            return redirect('/home');
         }
         
    }

     /**
     * 
     * Post Edit form admin 
     * */
    public function edit($id)
    {
        $posts = Post::Where('id',$id)->first();
        return view('posts.edit',compact('posts'));
    }

    /**
     * 
     * Update Post form admin 
     * */
    public function updatePost(Request $request, $id){
    
        $validate           = Validator::make($request->all(), [
            'photo'         => 'required',
            'title'         => 'required',
            'description'   =>'required'
         ])->validate();
         $posts             = Post::where('id',$id)->update([
             'image'        =>$this->uploadImage($request->photo),
             'title'        =>$request->title,
             'description'  =>$request->input('description')
         ]);
         if ($posts){
             return redirect('/home');
         } else {
            return redirect('/home');
         }
         
    }

     /**
     * 
     * Post Comments 
     * */
    public function postComments(Request $request)
    {
        $comments = Comment::create([
            'user_id'=> $request->{Auth::id()},
            'post_id'=>$request->post_id,
            'user_comment'=>$request->user_comment
        ]);
        if ($comments){
            return response()->json(['status'=>'success','comments'   => $comments]);
        } else {
            return response()->json(['status'=>'error']);
        }
    }


    public function postCreate(Request $request)
    {
         $post = new Post([
            'title' => $request->get('title'),
            'description' => $request->get('body')
          ]);
    
          $post->save();
    
          return response()->json('successfully added');
    }
}
