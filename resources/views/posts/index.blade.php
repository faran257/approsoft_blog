@extends('layouts.app')

@section('content')

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-primary text-white">{{ __('Posts') }}
                <a href="{{route('post-create')}}"><button class="btn btn-dark">Create Post</button></a>
                </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" onclick="checkAll(this)"></th>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            @foreach($posts as $post)
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name=""></td>
                                    <td>{{$post->id}}</td>
                                    <td><img src="../{{$post->image}}" alt="" height="50" width="50"></td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->description}}</td>
                                    <td>
                                       <a href="{{route('post-edit',$post->id)}}"><button class="btn btn-success text-white btn-sm">Edit</button></a>
                                       <a href=""><button class="btn btn-danger text-white btn-sm">Del</button></a>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                            <tfoot>
                                <tr>
                                
                                </tr>
                            </tfoot>
                        </table>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
