@extends('layouts.app')

@section('content')

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white">{{ __('Create Post Form') }}
                </div>
                <div class="card-body">
                  <form action="{{route('post-store')}}" method="POST"  enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <img id="blah" src="#" alt="your" />
                                <label for="recipient-name" class="col-form-label">Image:</label>
                                <input type="file" class="form-control"  name="photo" onchange="readURL(this);">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Title:</label>
                                <input type="text" class="form-control"  name="title">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Description:</label>
                                <textarea class="form-control"  name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn-info btn-block">Create Now</button>
                            </div>
                    </form>
                  </div>
           </div>
        </div>
    </div>
</div>

@endsection